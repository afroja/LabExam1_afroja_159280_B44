<html>
<head>
    <title>Chessboard with PHP, HTML and CSS </title>
    <style type="text/css">
        div {
            float: left;
            width: 20px;
            height: 20px;
            border-style: solid;
            border-width: 1px;
        }

        .black {
            background: black;
        }
        .white {
            background: white;
        }

    </style>

</head>
<body>

<?php
echo "<form action='' method='POST'>
       <input type='text' name='num' placeholder='Enter a number'>
       <input type='submit' value='send' >
       </form>";

$num = $_REQUEST['num'];


for ($i=0; $i < $num; $i++) {

    for($j=0; $j < $num; $j++){
        if($j%$num==0) echo "<br style=\"clear:both\" />";
        if($j%2==0){
            if($i%2==0)
            {
                echo "<div class='white'></div>";
            }
            else{
                echo "<div class='black'></div>";
            }
        }
        else{
            if($i%2!=0)
            {
                echo "<div class='white'></div>";
            }
            else{
                echo "<div class='black'></div>";
            }
        }

    }

}
?>
</body>
</html>
